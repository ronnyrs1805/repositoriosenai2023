const {createApp} = Vue;
createApp({
    data(){
        return{
            valorDisplay:"0",
            operador:null,
            numeroAtual:null,
            numeroAnterior:null,
            tamanhoLetra: 50+ "px",
             //login
             usuario: "",
             senha:"",
             erro:null,
             sucesso:null,

             usuarios:["admin", "ronny", "joana"],
             senhas:["1234", "1234", "1234"],
             userAdmin: false,

             newUsername: "",
             newPassword: "",
             confirmPassword: "",
             mostrarEntrada: false,
             //login
             //login
             mostrarLista: false,
             //login
        };//Fechamento return
    },//Fechamento data

    methods:{
        login(){
            // alert("Testando...");

            //Simuklando uma requisão de login assincrona
            setTimeout(() =>{
                if((this.usuario==="Ronny" && this.senha==="bomdia") || (this.usuario==="negão" && this.senha==="doido") || (this.usuario==="Tsunade" && this.senha==="naruto") || (this.usuario==="cabo" && this.senha==="fio")){
                    // alert("Login efetuado com sucesso!");
                    this.erro = null;
                    this.sucesso = "login efetuado com sucesso!"
                    setTimeout(function(){
                        window.location.href='calculadora.html';
                    },400);
                }
                else{
                    // alert("Usuario ou senha  incorretos");
                    this.erro= "Usuario ou senha incorretos!";
                    this.sucesso=null;
                }
            },400); 
        },//Fechamento Login
        getNumero(numero){
            if(this.valorDisplay == "0"){
                this.valorDisplay = numero.toString();
            }
            else{
                if (this.operador == "="){
                    this.valorDisplay = "";
                    this.operador=null;
                }
               //this.valorDisplay = this.valorDisplay + numero.toString();

                //adição simplificada
                this.valorDisplay += numero.toString();
            }
        },// Fechamento getNumero

        Limpar(){
            this.valorDisplay = "0";
            this.operador = null;
            this.numeroAnterior = null;
            this.numeroAtual = null;
        },//Fechamento limpar

        decimal(){
                if(!this.valorDisplay.includes(".")){
                    this.valorDisplay += ".";
                }//Fechamento if
        },//Fechamento decimal

        operacoes(operacao){
            if(this.numeroAtual != null){
                const displayAtual = parseFloat
                (this.valorDisplay);
                if(this.operador!= null){
                    switch(this.operador){
                        case"+":
                            this.valorDisplay = (this.numeroAtual + displayAtual).toString();
                            break;
                        
                        case"-":
                            this.valorDisplay = (this.numeroAtual - displayAtual).toString(); 
                            break;
                             
                        case"*":
                            this.valorDisplay = (this.numeroAtual*displayAtual).toString();
                            break;
                        case"/":
                            this.valorDisplay = (this.numeroAtual / displayAtual).toString();
                        if (displayAtual==0){this.valorDisplay = "não pode se dividir por 0";
                        }else{this.valorDisplay = (this.numeroAtual / displayAtual)}
                        break;
                    }//Fim do switch
                    this.numeroAnterior = this.numeroAtual
                    this.numeroAtual = null;
                    this.operador = null;
                     
                    if (this.operador != "="){
                        this.operador = null;
                    }

                    //acertar o número de casas decimais quando o resultado for decimal
                    if(this.valorDisplay.includes(".")){
                        const numDecimal = parseFloat(this.valorDisplay);
                        this.valorDisplay + (numDecimal.toFixed(2)). toString();

                    }
                }//Fim do if
                else{
                    this.numeroAnterior = displayAtual;
                }//Fim do else
            }//Fim do if numeroAtual
            this.operador = operacao;
            this.numeroAtual = parseFloat(this.valorDisplay);
            if(this.operador != "="){
                this.valorDisplay = "0";
            }
            //this.valorDisplay = "0";
            
        },//fim operações
        ajusteTamanhoDisplay(){
            if(this.valorDisplay.length >= 21){
                this.tamanhoLetra = 30 + "px";
            }
            // else if()
        },
        login2(){//verificação de usuário e senha cadastrados ou não nos arrays
            this.mostrarEntrada = false;
             setTimeout(()=> {

             }, 1000);
            const index = this.usuarios.indexOf(this.usuario);
            if(index !== -1 && this.senhas[index] === this.senha){
                this.erro = null;
                this.sucesso = "login efetuado com sucesso!"
                
                
                //Registrando o usuário no LocalStorage para Lembrete de acessos
                localStorage.setItem("usuario", this.usuario);
                localStorage.setItem("senha", this.senha);

                 //Verificando se o  usuário é admin
                if(this.usuario === "admin" && index === 0){
                    this.userAdmin = true;
                    this.sucesso = "Logado como ADMIN!";
                }
            }
            else{
                this.erro = "Usuário e / ou senha incorretos!";
                this.sucesso = null;
            }

            //Arrays (vetores) para armazenamento dos nomes de usu
           
        },//Fechamento login2
        
        paginaCadastro(){
            this.mostrarEntrada = false;
            if(this.userAdmin == true){
                this.erro = null;
                this.sucesso= "Carregando Página de Cadastro...";
                this.mostrarEntrada = true;
 
                //Espera estratégica antes do carregamento da página
                setTimeout(() =>{}, 2000);

                setTimeout(() =>{
                    window.location.href = "cadastro.html";
                },1000);
            }//Fechamento if
            else{
                this.sucesso = null;
                setTimeout(() =>{
                    this.mostrarEntrada = true;
                    this.erro = "Sem privilégios ADMIN!!!";
                }, 1000);
            }//Fechamento else
        },//Fechamento paginaCadastro

        adicionarUsuario(){
            this.mostrarEntrada= false;
            this.usuario= localStorage.getItem("usuario");
            this.senha= localStorage.getItem("senha");

            setTimeout(() => {
                this.mostrarEntrada = true;

                if(this.usuario === "admin"){
                    /*Verificando se o novo usuário é diferente de vazio e se ele já não está cadastrado no sistema*/
                    if(!this.usuarios.includes(this.newUsername)&& this.newUsername!== "" && !this.newUsername.includes(" ")){
                        this.sucesso = "Usuário válido"
                        this.erro = null;
                        //Validação da senha digitada para cadastro no array
                        if(this.newPassword !== "" && !this.newPassword.includes(" ") && this.newPassword === this.confirmPassword){
                            //Inserindo o novo usuário e senha arrays
                            this.usuarios.push(this.newUsername);
                            this.senhas.push(this.newPassword);
                            //Atualizando o usuário recém cadastrado no LocalStorage
                            localStorage.setItem("usuarios",JSON.stringify(this.usuarios));
                            localStorage.setItem("senhas",JSON.stringify(this.senhas));
                            
                            this.newUsername = "";
                            this.newPassword = "";
                            this.confirmPassword = "";

                            this.erro = null;
                            this.sucesso = "Usuário cadastrado com sucesso";
                        }//Fechamento if password
                        else{
                            this.sucesso = null;
                            this.erro = "Por favor, informe uma senha válida!!!";

                            this.newUsername = "";
                            this.newPassword = "";
                            this.confirmPassword = "";

                        }
                    }//Fechamento if includes
                    else{
                        this.erro = "Usuário já cadastrado ou inválido! Por favor digite um usuário diferente!";
                        this.sucesso = null;
                    }//Fechamento else
                }//Fechamento if admin
                else{
                    this.erro = "Não está logado como ADMIN!"
                    this.sucesso = null;

                    this.newUsername = "";
                    this.newPassword = "";
                    this.confirmPassword = "";
                }//Fechamento else admin
            },1000);

        },//Fechamento adicicionarUsuario

        listarUsuarios(){
            if(localStorage.getItem("usuarios") && localStorage.getItem("senhas")){
                this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
                this.senhas = JSON.parse(localStorage.getItem("senhas"));
            }//Fechamento if
            this.mostrarLista = !this.mostrarLista;
        },//Fechamento listarUsuarios

        excluirUsuario(usuario){
            this.mostrarEntrada = false;
            if(usuario === "admin"){
               setTimeout(() => {
                this.mostrarEntrada = true;
                this.sucesso = null;
                this.erro = "O usuário ADMIN não pode ser excluído!!!"
               }, 500);
               return;//Força a saída deste bloco
                }//fechamento do if

                if(confirm("Tem certeza que deseja excluir o usuário?")){
                    const index = this.usuarios.indexOf(usuario);
                    if(index != -1){
                        this.usuarios.splice(index, 1);
                        this.senhas.splice(index, 1);

                        //Atualiza o array usuarios no Localstorage
                        localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
                        localStorage.setItem("senhas", JSON.stringify(this.senhas));

                        setTimeout(() => {
                            this.mostrarEntrada = true;
                            this.erro = null;
                            this.sucesso = "Usuário excluido com sucesso!";
                        }, 500);
                    }//Fechamento if index
                }//Fechamento if
        },//Fechamento excluirUsuario

    },//Fechamento methods

}).mount("#app");//Fechamento app