const {createApp} = Vue;
createApp({
    data(){
        return{
            valorDisplay:"0",
            operador:null,
            numeroAtual:null,
            numeroAnterior:null,
            tamanhoLetra: 50+ "px",
        };//Fechamento return
    },//Fechamento data

    methods:{
        getNumero(numero){
            if(this.valorDisplay == "0"){
                this.valorDisplay = numero.toString();
            }
            else{
                if (this.operador == "="){
                    this.valorDisplay = "";
                    this.operador=null;
                }
               //this.valorDisplay = this.valorDisplay + numero.toString();

                //adição simplificada
                this.valorDisplay += numero.toString();
            }
        },// Fechamento getNumero

        Limpar(){
            this.valorDisplay = "0";
            this.operador = null;
            this.numeroAnterior = null;
            this.numeroAtual = null;
        },//Fechamento limpar

        decimal(){
                if(!this.valorDisplay.includes(".")){
                    this.valorDisplay += ".";
                }//Fechamento if
        },//Fechamento decimal

        operacoes(operacao){
            if(this.numeroAtual != null){
                const displayAtual = parseFloat
                (this.valorDisplay);
                if(this.operador!= null){
                    switch(this.operador){
                        case"+":
                            this.valorDisplay = (this.numeroAtual + displayAtual).toString();
                            break;
                        
                        case"-":
                            this.valorDisplay = (this.numeroAtual - displayAtual).toString(); 
                            break;
                             
                        case"*":
                            this.valorDisplay = (this.numeroAtual*displayAtual).toString();
                            break;
                        case"/":
                            this.valorDisplay = (this.numeroAtual / displayAtual).toString();
                        if (displayAtual==0){this.valorDisplay = "não pode se dividir por 0";
                        }else{this.valorDisplay = (this.numeroAtual / displayAtual)}
                        break;
                    }//Fim do switch
                    this.numeroAnterior = this.numeroAtual
                    this.numeroAtual = null;
                    this.operador = null;
                     
                    if (this.operador != "="){
                        this.operador = null;
                    }

                    //acertar o número de casas decimais quando o resultado for decimal
                    if(this.valorDisplay.includes(".")){
                        const numDecimal = parseFloat(this.valorDisplay);
                        this.valorDisplay + (numDecimal.toFixed(2)). toString();

                    }
                }//Fim do if
                else{
                    this.numeroAnterior = displayAtual;
                }//Fim do else
            }//Fim do if numeroAtual
            this.operador = operacao;
            this.numeroAtual = parseFloat(this.valorDisplay);
            if(this.operador != "="){
                this.valorDisplay = "0";
            }
            //this.valorDisplay = "0";
            
        },//fim operações
        ajusteTamanhoDisplay(){
            if(this.valorDisplay.length >= 21){
                this.tamanhoLetra = 30 + "px";
            }
            // else if()
        }
        
    },//Fechamento methods

}).mount("#app");//Fechamento app